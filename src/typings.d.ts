// # 3rd Party Library
// If the library doesn't have typings available at `@types/`,
// you can still use it by manually adding typings for it

interface Device {
    api: string,
    property: {
        title: string,
        description: string
    },
    config: {
        borderless: boolean
    },
    cover: string
}

interface DeviceInfo {
    name: string,
    option: import("@delon/chart/chart-echarts").ChartEChartsOption,
    on: import("@delon/chart/chart-echarts").ChartEChartsOn[]
}
