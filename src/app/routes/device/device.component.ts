import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ChartEChartsOn, ChartEChartsOption } from '@delon/chart/chart-echarts';
import { Subscription } from 'rxjs';
import { DeviceService } from './device.service';

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  styles: [
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeviceComponent implements OnInit {

  option: ChartEChartsOption = {};
  on: ChartEChartsOn[] = [];

  deviceData$: Subscription = new Subscription();

  constructor(
    private cdr: ChangeDetectorRef,
    private deviceService: DeviceService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.deviceData$ = this.deviceService
        .getDeviceData(params.get('name') as string)
        .subscribe(res => {
          if (res.msg !== 'ok') {
            this.option = res?.[0].option;
            this.on = res?.[0].on;
            this.cdr.detectChanges();
          }
        });
    });
  }

  ngOnDestroy() {
    this.deviceData$.unsubscribe();
  }

}