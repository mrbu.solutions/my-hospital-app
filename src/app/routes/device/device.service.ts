import { Injectable } from '@angular/core';
import { _HttpClient } from '@delon/theme';

@Injectable({ providedIn: 'root' })
export class DeviceService {

  constructor(private http: _HttpClient) { }

  getDeviceData(name: string) {
    return this.http.get(`/device/${name}`);
  }
}
