import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { DevicesService } from './devices.service';

@Component({
  selector: 'app-devices',
  templateUrl: './devices.component.html',
  styles: [
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DevicesComponent implements OnInit {

  devices: Array<Device> = [];
  devices$: Subscription = new Subscription();

  gridStyle = {
    width: '25%',
    textAlign: 'center'
  };

  constructor(
    private cdr: ChangeDetectorRef,
    private devicesService: DevicesService,
    private router: Router
  ) { }

  ngOnInit(): void {

    this.devices$ = this.devicesService
      .getDeviceInfo()
      .subscribe(res => {
        if (res.msg !== 'ok') {
          this.devices = res;
          console.log(res);
          this.cdr.detectChanges();
        }
      });
  }

  ngOnDestroy() {
    this.devices$.unsubscribe();
  }

  showDeviceData(api: string) {
    this.router.navigate(['/device', api]);
  }

}
