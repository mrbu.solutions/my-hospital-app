import { NgModule, Type } from '@angular/core';
import { SharedModule } from '@shared';

import { DevicesComponent } from './devices.component';
import { DevicesService } from './devices.service';

const COMPONENTS: Type<void>[] = [
  DevicesComponent
];

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: COMPONENTS,
  providers: [
    DevicesService
  ],
  exports: [
    DevicesComponent
  ]
})
export class DevicesModule { }
