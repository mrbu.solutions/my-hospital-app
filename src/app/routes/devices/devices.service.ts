import { Injectable } from '@angular/core';
import { _HttpClient } from '@delon/theme';

@Injectable({ providedIn: 'root' })
export class DevicesService {

  constructor(private http: _HttpClient) { }

  getDeviceInfo() {
    return this.http.get('/device');
  }

}
