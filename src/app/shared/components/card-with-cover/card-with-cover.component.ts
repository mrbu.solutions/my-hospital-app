import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-card-with-cover',
  templateUrl: './card-with-cover.component.html',
  styles: [
  ]
})
export class CardWithCoverComponent implements OnInit {
  @Input() card: Device = <Device>{};

  constructor() { }

  ngOnInit(): void {
  }

}
