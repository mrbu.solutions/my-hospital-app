import { MockRequest } from '@delon/mock';

const DEVICE_INFO: Array<DeviceInfo> = [
  {
    name: 'spo2',
    option: {
      // Make gradient line here
      visualMap: [
        {
          show: true,
          type: 'continuous',
          seriesIndex: 0,
          min: 90,
          max: 100
        }
      ],
      title: [
        {
          left: 'center',
          text: 'Oxygen Saturation Level'
        }
      ],
      tooltip: {
        trigger: 'axis'
      },
      xAxis: [
        {
          data: ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00']
        }
      ],
      yAxis: [
        {}
      ],
      grid: [
        {
          bottom: '10%'
        }
      ],
      series: [
        {
          type: 'line',
          showSymbol: true,
          data: ['92', '96', '98', '94', '92', '94', '96', '97', '97', '98', '96', '97', '99', '99']
        }
      ]
    },
    on: []
  },
  {
    name: 'bloodpressure',
    option: {
      color: ['#5470C6', '#EE6666'],
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          //type: 'cross'
        }
      },
      legend: {},
      grid: {
        top: 70,
        bottom: 50
      },
      xAxis: [
        {
          type: 'category',
          axisTick: {
            alignWithLabel: true
          },
          axisLine: {
            onZero: false,
            lineStyle: {
              color: '#5470C6'
            }
          },
          // prettier-ignore
          data: ['2016-1', '2016-2', '2016-3', '2016-4', '2016-5', '2016-6', '2016-7', '2016-8', '2016-9', '2016-10', '2016-11', '2016-12']
        },
        {
          type: 'category',
          axisTick: {
            alignWithLabel: true
          },
          axisLine: {
            onZero: false,
            lineStyle: {
              color: '#5470C6'
            }
          },
          // prettier-ignore
          data: []
        }
      ],
      yAxis: [
        {
          type: 'value'
        }
      ],
      series: [
        {
          name: 'Systolic',
          type: 'line',
          xAxisIndex: 1,
          smooth: true,
          emphasis: {
            focus: 'series'
          },
          data: [
            100, 120, 100, 90, 120, 130, 140, 130, 120, 130, 120, 110
          ]
        },
        {
          name: 'Diastolic',
          type: 'line',
          smooth: true,
          emphasis: {
            focus: 'series'
          },
          data: [
            70, 80, 80, 70, 80, 90, 100, 100, 90, 80, 80, 70
          ]
        }
      ]
    },
    on: [],
  },
  {
    name: 'ecg',
    option: {
      // Make gradient line here
      visualMap: [
        {
          show: false,
          type: 'continuous',
          seriesIndex: 0,
          min: 40,
          max: 80
        }
      ],
      title: [
        {
          left: 'center',
          text: 'ECG'
        }
      ],
      xAxis: [
        {
          data: []
        }
      ],
      yAxis: [
        {}
      ],
      grid: [
        {
          bottom: '10%'
        }
      ],
      series: [
        {
          type: 'line',
          showSymbol: false,
          data: ['60', '60', '70', '60', '92', '40', '70', '50', '70', '50', '60', '60', '60', '60', '60', '60', '70', '60', '92', '40', '70', '50', '70', '50', '60', '60', '60', '60']
        }
      ]
    },
    on: [],
  },
  {
    name: 'temperature',
    option: {
      title: [
        {
          left: 'center',
          text: 'Body Temperature'
        }
      ],
      tooltip: {
        trigger: 'axis'
      },
      xAxis: {
        type: 'category',
        data: ['10:00 AM', '11:00 AM', '12:00 PM', '1:00 PM', '2:00 PM', '3:00 PM', '4:00 PM']
      },
      yAxis: {
        type: 'value'
      },
      series: [
        {
          data: [37, 38, 38.5, 39, 38, 37, 35],
          type: 'line'
        }
      ]
    },
    on: [],
  },
  {
    name: 'bmi',
    option: {
      title: [{
        text: 'BMI'
      }],
      tooltip: {
        trigger: 'axis'
      },
      legend: {
        data: ['BMI', 'Body Fat (%)', 'Weight (kg)']
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
      },
      toolbox: {
        feature: {
          saveAsImage: {}
        }
      },
      xAxis: {
        type: 'category',
        boundaryGap: false,
        data: ['Week 1', 'Week 2', 'Week 3', 'Week 4', 'Week 5', 'Week 6', 'Week 7']
      },
      yAxis: {
        type: 'value'
      },
      series: [
        {
          name: 'BMI',
          type: 'line',
          stack: 'Total',
          data: [30, 30, 28, 28, 27, 26, 25]
        },
        {
          name: 'Body Fat (%)',
          type: 'line',
          stack: 'Total',
          data: [28, 27, 26, 26, 26, 25, 24]
        },
        {
          name: 'Weight (kg)',
          type: 'line',
          stack: 'Total',
          data: [80, 83, 79, 78, 78, 75, 70]
        }
      ]
    },
    on: [],
  },
  {
    name: 'glucose',
    option: {
      title: [{
        text: 'Glucose Level (mg/dL)',
        left: 'center'
      }],
      tooltip: {
        trigger: 'axis'
      },
      xAxis: {
        type: 'category',
        data: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul']
      },
      yAxis: {
        type: 'value'
      },
      series: [
        {
          data: [150, 120, 130, 115, 130, 147, 120],
          type: 'line'
        }
      ]
    },
    on: [],
  }
]

function getDeviceInfo(name?: string) {
  return DEVICE_INFO.filter(device => device.name === name);
}

function listDevice(): Array<Device> {
  return [
    {
      api: 'spo2',
      property: {
        title: "SPo2",
        description: "Blood Saturation Level History",
      },
      config: {
        borderless: true
      },
      cover: "https://cdn.vectorstock.com/i/1000x1000/45/71/pulse-oximeter-icon-vector-12534571.webp"
    },
    {
      api: 'bloodpressure',
      property: {
        title: "Blood Pressure",
        description: "Blood Pressure History",
      },
      config: {
        borderless: true
      },
      cover: "https://cdn.vectorstock.com/i/1000x1000/69/43/high-blood-pressure-concept-vector-10386943.webp"
    },
    {
      api: 'ecg',
      property: {
        title: "ECG",
        description: "ECG History",
      },
      config: {
        borderless: true
      },
      cover: "https://cdn.vectorstock.com/i/1000x1000/99/05/ecg-vector-39119905.webp"
    },
    {
      api: 'temperature',
      property: {
        title: "Temperature",
        description: "Temperature History",
      },
      config: {
        borderless: true
      },
      cover: "https://cdn.vectorstock.com/i/1000x1000/17/25/body-temperature-check-man-is-being-checked-vector-35041725.webp"
    },
    {
      api: 'bmi',
      property: {
        title: "BMI",
        description: "BMI History",
      },
      config: {
        borderless: true
      },
      cover: "https://cdn.vectorstock.com/i/1000x1000/08/33/female-body-mass-index-normal-weight-obesity-vector-22070833.webp"
    },
    {
      api: 'glucose',
      property: {
        title: "Glucose Level",
        description: "Glucose Level History",
      },
      config: {
        borderless: true
      },
      cover: "https://cdn.vectorstock.com/i/1000x1000/75/91/blood-glucose-meter-in-hand-vector-13617591.webp"
    }
  ]
}

export const DEVICES = {
  '/device': () => listDevice(),
  '/device/:name': (req: MockRequest) => getDeviceInfo(req.params.name),
};
